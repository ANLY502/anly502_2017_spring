#!/usr/bin/env python34
# 
# Analyze forensics wiki input file
# For information on MRJOB see:
# https://pythonhosted.org/mrjob/guides/quickstart.html

import mrjob
from mrjob.job import MRJob
import re

CLR_RE = re.compile(r'^(\S+) (\S+) (\S+) \[([^\]]+)\] "(\S+) (\S+) \S+" (\S+) (\S+) "([^"]*)" "([^"]*)"')

class LogLine(object):
    __slots__ = ['ipaddr','datetime','verb','path','code','bytes','refer','user_agent']
    def __init__(self,line):
        import datetime
        m = CLR_RE.search(line)
        if m:
            self.ipaddr     = m.group(1)
            self.datetime   = datetime.datetime.strptime(m.group(4),"%m/%b/%Y:%H:%M:%S %z")
            self.verb       = m.group(5)
            self.path       = m.group(6)
            self.code       = int(m.group(7))
            self.bytes      = int(m.group(8))
            self.refer      = m.group(9)
            self.user_agent = m.group(10)
        else:
            self.ipaddr     = None
            self.datetime   = None
            self.verb       = None
            self.path       = None
            self.code       = None
            self.bytes      = None
            self.refer      = None
            self.user_agent = None
        

# This job accepts the basic log files and outputs 
class FWikiAnalyzer(MRJob):
    SORT_VALUES = True
    def mapper(self, _, line):
        w = LogLine(line)
        try:
            if w.ipaddr:
                yield ( w.path, w.bytes ), w.datetime.isoformat()
            else:
                self.increment_counter("Info","CLR_RE Unmatched Lines",1)
        except RuntimeError as e:
            self.increment_counter("Error","RuntimeError in FWikiAnalyzer.mapper",1)
            pass

    def reducer(self, url_count, ones):
        yield url_count, sum((1 for x in ones))


if __name__ == '__main__':
    FWikiAnalyzer.run()

