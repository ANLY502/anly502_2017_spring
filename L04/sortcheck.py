#!/usr/bin/env python34
#
# Verify sort order
#
if __name__=="__main__":
    import sys,json
    for fname in sys.argv[1:]:
        a0 = None
        linenumber = 0
        for line in open(fname,"rU"):
            linenumber += 1
            (k,v) = line.strip().split("\t")
            a = json.loads(k)
            if a0 and a[0] < a0[0]:
                print("{}: {}".format(linenumber-1,a0))
                print("{}: {}".format(linenumber,a))
            a0 = a0

