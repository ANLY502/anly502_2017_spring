# README #

Welcome to ANLY502, Spring 2017.

### What is this repository for? ###

This is the main class GIT repository. It contains:

* Class notes (that we don't put on Google Drive)
* Problem Sets
* Starter code for each problem set.

This file is formatted in [markdown](https://bitbucket.org/tutorials/markdowndemo).

This repository is located on both BitBucket and Github at:


* Bitbucket: [https://ANLY502@bitbucket.org/ANLY502/anly502_2017_spring.git][1]
* Github: [https://github.com/anly502/anly502_2017_spring.git][2]

[1]: https://ANLY502@bitbucket.org/ANLY502/anly502_2017_spring.git
[2]: https://github.com/anly502/anly502_2017_spring.git

You should ***[fork][3]*** this repository into a *private* repository. You
can do this with free account on Bitbucket, and with an educational
account on Github. Alternatively, you can simply ***clone*** this
repository, pull your updates from the repository as necessary, and
push changes back to your own server.

[3]: https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html

In ANLY502 you will be submitting a ZIP file that contains your code
and results. This repository also includes a *submission verification
script* that you can use to test your code. 

### What do I do next? ###

You can review:

* [Lecture Notes for class #1][5] (L01)
* [Assignment #1][6] (A1), due 

[5]: L01/
[6]: A1/

### How do I get set up? ###

To get started in this course, you need to understand:

* How do fork (or clone) this repository.
* How to clone the repository to your laptop.
* How to commit your changes and push them back to your *private repository*.

