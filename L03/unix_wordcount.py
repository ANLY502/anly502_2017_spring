#!/usr/bin/env python34

from mrjob.job import MRJob
class SumOfSquares(MRJob):

    def mapper(self, _, line):
        yield "chars",len(line)
        yield "lines",1
        yield "words",len(line.split(" "))

    def reducer(self, key, values):
        yield key, sum(values)


if __name__ == '__main__':
    SumOfSquares()
