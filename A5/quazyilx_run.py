#!/usr/bin/env python3
#
# Run this program with spark-submit

import sys,os,datetime,re,operator
from pyspark import SparkContext, SparkConf

INSERT_YOUR_CODE_HERE = ""

QUERIES = [["total_rows","select count(*) from quazyilx"],
           ["total_errors",INSERT_YOUR_CODE_HERE], # fnard==fnork==cark==-1
           ["one_error_others_gt5",INSERT_YOUR_CODE_HERE], # fnard==-1, fnok>5, 
           ["first_date",INSERT_YOUR_CODE_HERE],   
           ["last_date",INSERT_YOUR_CODE_HERE],
           ["first_error_date",INSERT_YOUR_CODE_HERE],
           ["last_error_date",INSERT_YOUR_CODE_HERE]
]



if __name__=="__main__":
    # Get your sparkcontext and make a dataframe
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.appName("quazyilx").getOrCreate()
    sc    = spark.sparkContext      # get the context
    
    # Replace this code with your own
    print("*** Verifying that Spark works ***",file=sys.stderr)
    res = sc.parallelize(range(1,1000)).reduce(operator.add)
    print("*** Result = {}  (should be 499500) ***".format(res),file=sys.stderr)
    assert res==499500

    # Create an RDD from s3://gu-anly502/A1/quazyilx1.txt
    # NOTE: Do this with 1 master m3.xlarge, 2 core m3.xlarge, and 4 task m3.xlarge
    # otherwise it will take forever...
    
    # register your dataframe as the SQL table quazyilx
    # You probably want to cache it, also!

    # Print how many rows we have
    print("rows: {}".format(spark.sql("select count(*) from quazyilx").collect()))

    # Now do the queries
    for (var,query) in QUERIES:
        print("{}-query: {}".format(var,query))
        if query:
            query_result = spark.sql(query).collect()
            print("{}: {}".format(var,query_result))
