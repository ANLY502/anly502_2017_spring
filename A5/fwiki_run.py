#!/usr/bin/env python3
#
# Run this program with spark-submit

import sys,os,datetime,re,operator
from pyspark import SparkContext, SparkConf



if __name__=="__main__":
    # Get your sparkcontext and make a dataframe
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.appName("quazyilx").getOrCreate()
    sc    = spark.sparkContext      # get the context
    
    # Create an RDD from s3://gu-anly502/logs/forensicswiki.2012.txt
    url = "s3://gu-anly502/logs/forensicswiki.2012.txt"
    # NOTE: Do this with 1 master m3.xlarge, 2 core m3.xlarge, and 4 task m3.xlarge
    # otherwise it will take forever...
    
    loglines = <<<insert code to read the logfiles into a RDD >>>
    logs     = <<<insert code to convert the loglines into an RDD of Row() records >>>
    df       = <<<insert code to convert logs into a DataFrame using spark.createDataFrame() >>>
    
    # Register the dataframe as an SQL table called 'logs'

    # Print how many log lines there are
    print("Total Log Lines: {}".format(spark.sql("select count(*) from log").collect()))

    # Figure out when it started and ended
    (start,end) = spark.sql("select min(datetime),max(datetime) from log").collect()[0]

    print("Date range: {} to {}".format(start,end))

    # Now generate the requested output
