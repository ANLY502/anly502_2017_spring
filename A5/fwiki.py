import sys
import os,datetime,re

# We are giving you the regular expression!

CLR_RE = re.compile(r'^(\S+) (\S+) (\S+) \[([^\]]+)\] "(\S+) (\S+) \S+" (\S+) (\S+) "([^"]*)" "([^"]*)"')
log_re = re.compile(CLR_RE)

class LogLine():
    __slots__ = ['ipaddr','path','method','datetime','result','bytes','refer','agent']
    def __init__(self,line):
        # Parse line with the regular expression
        m = log_re.search(line)
        if not m:
            self.ipaddr   = None
            self.method   = None
            self.path     = None
            self.datetime = None
            self.result   = None
            self.bytes    = None
            self.refer    = None
            self.agent    = None
            return
        self.ipaddr     = m.group(1)
        self.datetime   = datetime.datetime.strptime(m.group(4),"%d/%b/%Y:%H:%M:%S %z")
        # You need to fill in the rest...

    def __str__(self):
        return "{} {} {} {} {} {} {} {}".format(self.ipaddr,self.method,self.path,self.datetime.isoformat(),self.result,self.bytes,self.refer,self.agent)

    def __repr__(self):
        return "<LogLine: {} {} {} {} {} {} {} {}>".format(self.ipaddr,self.method,self.path,self.datetime.isoformat(),self.result,self.bytes,self.refer,self.agent)

    def row(self):
        from pyspark.sql import Row
        # You need to fill in the rest:
        return Row(ipaddr=self.ipaddr,datetime=self.datetime)
        
