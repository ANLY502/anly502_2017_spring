#!/usr/bin/python3.4
#
# Template to compute the number of distinct IP addresses 
#

from mrjob.job import MRJob
import re

class DistinctIPAddresses(MRJob):

    def mapper(self, _, line):
        # Your code goes here

    def reducer(self, word, counts):
        # Your code goes here


if __name__ == '__main__':
    DistinctIPAddresses.run()
